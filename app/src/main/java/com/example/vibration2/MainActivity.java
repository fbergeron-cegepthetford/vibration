package com.example.vibration2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private RadioButton rbCourt, rbLong, rbDeuxCourts;
    private RadioButton rbDeuxLongs, rbCourtLong, rbLongCourt;
    private Button btVibrer;
    private HashMap<RadioButton, long[]> dureesVibration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.initialiserVue();
        this.initialiserCorrespondanceVibrations();
    }

    private void initialiserVue() {
        this.setContentView(R.layout.activity_main);

        rbCourt = this.findViewById(R.id.rbCourt);
        rbLong = this.findViewById(R.id.rbLong);
        rbDeuxCourts = this.findViewById(R.id.rbDeuxCourts);
        rbDeuxLongs = this.findViewById(R.id.rbDeuxLongs);
        rbCourtLong = this.findViewById(R.id.rbCourtLong);
        rbLongCourt = this.findViewById(R.id.rbLongCourt);
        btVibrer = this.findViewById(R.id.btVibrer);

        rbCourt.setOnClickListener(this);
        rbLong.setOnClickListener(this);
        rbDeuxCourts.setOnClickListener(this);
        rbDeuxLongs.setOnClickListener(this);
        rbCourtLong.setOnClickListener(this);
        rbLongCourt.setOnClickListener(this);
        btVibrer.setOnClickListener(this);

        btVibrer.setEnabled(false);
    }

    private void initialiserCorrespondanceVibrations() {
        dureesVibration = new HashMap<RadioButton, long[]>();
        dureesVibration.put(rbCourt, new long[]{0, 500});
        dureesVibration.put(rbLong, new long[]{0, 1000});
        dureesVibration.put(rbDeuxCourts, new long[]{0, 500, 300, 500});
        dureesVibration.put(rbDeuxLongs, new long[]{0, 1000, 300, 1000});
        dureesVibration.put(rbCourtLong, new long[]{0, 500, 300, 1000});
        dureesVibration.put(rbLongCourt, new long[]{0, 1000, 300, 500});
    }

    private void preparerCanalNotification(String nomCanal, long[] dureeVibration) {
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channel = new NotificationChannel(""+nomCanal,
                "420-3V4-RA",
                NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription("Programmation mobile");
        channel.enableVibration(true);
        channel.setVibrationPattern(dureeVibration);
        notificationManager.createNotificationChannel(channel);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btVibrer:
                RadioButton boutonAppuye =
                        rbCourt.isChecked() ? rbCourt :
                                rbLong.isChecked() ? rbLong :
                                        rbDeuxCourts.isChecked() ? rbDeuxCourts :
                                                rbDeuxLongs.isChecked() ? rbDeuxLongs :
                                                        rbCourtLong.isChecked() ? rbCourtLong :
                                                                rbLongCourt;
                this.desactiverBoutonsRadio();
                btVibrer.setEnabled(false);
                long[] dureeVibration = dureesVibration.get(boutonAppuye);
                String motif = boutonAppuye.getText().toString();
                this.preparerCanalNotification(motif, dureeVibration);
                this.notifier(motif);
                break;
            case R.id.rbCourt:
            case R.id.rbLong:
            case R.id.rbDeuxCourts:
            case R.id.rbDeuxLongs:
            case R.id.rbCourtLong:
            case R.id.rbLongCourt:
                this.desactiverBoutonsRadio();
                btVibrer.setEnabled(true);
                ((RadioButton) view).setChecked(true);
                break;
        }
    }

    private void notifier(String nomCanal) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this, ""+nomCanal);

        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle(this.getTitle());
        builder.setContentText(nomCanal);

        NotificationManager nm =
                (NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);

        Notification notification = builder.build();
        nm.notify(0, notification);
    }

    private void desactiverBoutonsRadio() {
        rbCourt.setChecked(false);
        rbLong.setChecked(false);
        rbDeuxCourts.setChecked(false);
        rbDeuxLongs.setChecked(false);
        rbCourtLong.setChecked(false);
        rbLongCourt.setChecked(false);
    }
}
